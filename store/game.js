import { SETTINGS } from '~/utils/const'

const state = () => ({
  game: {
    name: 'Fifa 19',
    settings: SETTINGS,
    playersOnline: 4021,
  },
  players: [{}],
  isMatched: false,
  renderMatchmakingLobbyOnce: false,
  renderGameModalOnce: false,
  showGameModal: false,
})

const mutations = {
  UPDATE_SETTINGS(state, updatedSettings) {
    state.game.settings[updatedSettings.tab.title].forEach((opt) => {
      opt.id === updatedSettings.option.id
        ? (opt.selected = true)
        : (opt.selected = false)
    })
  },
  UPDATE_MATCH_STATUS(state, result) {
    state.isMatched = result
  },
  SAVE_NEW_PLAYER(state, player) {
    state.players.splice(0)
    state.players.push(player)
  },
  RENDER_GAME_MODAL_TEMPLATE(state) {
    state.renderGameModalOnce = true
  },
  RENDER_MATCHMAKING_LOBBY_TEMPLATE(state) {
    state.renderMatchmakingLobbyOnce = true
  },
  CHANGE_GAME_MODAL_VISIBILITY(state) {
    state.showGameModal = !state.showGameModal
  },
}

const actions = {
  updateSettings({ commit }, settings) {
    commit('UPDATE_SETTINGS', settings)
  },
  updateMatch({ commit }, result) {
    commit('UPDATE_MATCH_STATUS', result)
  },
  savePlayer({ commit }, player) {
    commit('SAVE_NEW_PLAYER', player)
  },
  renderGameModalTemplate({ commit }) {
    commit('RENDER_GAME_MODAL_TEMPLATE')
  },
  renderMatchmakingLobbyTemplate({ commit }) {
    commit('RENDER_MATCHMAKING_LOBBY_TEMPLATE')
  },
  changeGameModalVisibility({ commit }) {
    commit('CHANGE_GAME_MODAL_VISIBILITY')
  },
}

const getters = {
  game: (state) => state.game,
  players: (state) => state.players,
  matchIsFound: (state) => state.isMatched,
  gameModalTemplate: (state) => state.renderGameModalOnce,
  matchmakingLobbyTemplate: (state) => state.renderMatchmakingLobbyOnce,
  gameModal: (state) => state.showGameModal,
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
