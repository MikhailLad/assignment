import { LOBBIES } from '~/utils/const'

const state = () => ({
  lobby: LOBBIES.personal,
})

const mutations = {
  SHOW_LOBBY(state, lobby) {
    state.lobby = lobby
  },
}

const actions = {
  changeLobby({ commit }, lobby) {
    commit('SHOW_LOBBY', lobby)
  },
}

const getters = {
  lobby: (state) => state.lobby,
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
