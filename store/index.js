import game from '~/store/game'
import lobby from '~/store/lobby'
import user from '~/store/user'

const modules = {
  game,
  user,
  lobby,
}

export default {
  modules,
}
