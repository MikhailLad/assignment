const state = () => ({
  user: {
    name: 'steveroesler',
    src: require('~/assets/images/avatars/avatar1.png'),
    srcset: require('~/assets/images/avatars/avatar1.webp'),
    position: 'Team Leader',
    ready: true,
    level: 74,
  },
  renderUserModalOnce: false,
  showUserModal: false,
})

const mutations = {
  RENDER_USER_MODAL_TEMPLATE(state) {
    state.renderUserModalOnce = true
  },
  CHANGE_USER_MODAL_VISIBILITY(state) {
    state.showUserModal = !state.showUserModal
  },
}

const actions = {
  renderUserModalTemplate({ commit }) {
    commit('RENDER_USER_MODAL_TEMPLATE')
  },
  changeUserModalVisibility({ commit }) {
    commit('CHANGE_USER_MODAL_VISIBILITY')
  },
}

const getters = {
  user: (state) => state.user,
  userModalTemplate: (state) => state.renderUserModalOnce,
  userModal: (state) => state.showUserModal,
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
