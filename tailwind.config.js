/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    extend: {
      maxHeight: {
        '0': 0,
      },
      minWidth: {
        '10': '42px',
      },
      zIndex: {
        '1': 1,
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
      },
      width: {
        '19/20': '95%',
        '14': '56px',
      },
      height: {
        'xs': '2px',
        '14': '56px',
      },
      lineHeight: {
        '11': '44px'
      },
    }
  },
  variants: {},
  plugins: []
}
