import { NUMBER_FORMATTER_REGEX, STRING_FORMATTER_REGEX } from '~/utils/const'

export default {
  methods: {
    formatNum(num) {
      if (typeof num !== 'number' && typeof num !== 'string')
        throw new Error('only numbers and strings allowed')
      return num.toString().replace(NUMBER_FORMATTER_REGEX, ',')
    },
    formatString(str) {
      if (typeof str !== 'string' || !str)
        throw new Error('only strings allowed')
      str = str
        .split(STRING_FORMATTER_REGEX)
        .map((word) => word.slice(0, 1).toUpperCase() + word.slice(1))
        .join(' ')
      return str
    },
  },
}
