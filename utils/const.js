export const SETTINGS = {
  type: [
    {
      id: 0,
      name: 'Competitive',
      selected: true,
    },
    {
      id: 1,
      name: 'Friendly',
      selected: false,
    },
    {
      id: 2,
      name: 'Online',
      selected: false,
    },
  ],
  gameMode: [
    {
      id: 0,
      name: 'Squad Battles',
      selected: true,
    },
    {
      id: 1,
      name: 'Ultimate Team',
      selected: false,
    },
    {
      id: 2,
      name: 'Kick Off',
      selected: false,
    },
    {
      id: 3,
      name: 'Skill Games',
      selected: false,
    },
    {
      id: 4,
      name: 'Practice Arena',
      selected: false,
    },
  ],
  lobbyStatus: [
    {
      id: 0,
      name: 'Invite Only',
      selected: true,
    },
    {
      id: 1,
      name: 'Visible For Everyone',
      selected: false,
    },
  ],
}

export const MATCHMAKING_LOBBY = {
  search: {
    title: 'Searching for compatible opponents',
    text:
      "We're searching our database of players to pair you with an opponent of similar skill",
    button: {
      status: 'Searching...',
      time: 120,
    },
  },
  matched: {
    title: 'A match has been found for you!',
    text:
      "We've found a compatible opponent for you to play Squad Battles with in FIFA 19",
    consent:
      'By tapping accept, you agree to play a match that may last for up to 30 minutes.',
    button: {
      status: 'Accept',
      time: 20,
    },
  },
}

export const LOBBIES = {
  personal: 'PERSONAL',
  matchmaking: 'MATCHMAKING',
}

export const NUMBER_FORMATTER_REGEX = /\B(?=(\d{3})+(?!\d))/g
export const STRING_FORMATTER_REGEX = /(?=[A-Z])/

export const AVATARS = [
  {
    id: 0,
    src: require('~/assets/images/avatars/avatar1.png'),
    srcset: require('~/assets/images/avatars/avatar1.webp'),
    taken: true,
    class: '_avatar1',
  },
  {
    id: 1,
    src: require('~/assets/images/avatars/avatar2.png'),
    srcset: require('~/assets/images/avatars/avatar2.webp'),
    taken: false,
    class: '_avatar2',
  },
  {
    id: 2,
    src: require('~/assets/images/avatars/avatar3.png'),
    srcset: require('~/assets/images/avatars/avatar3.webp'),
    taken: false,
    class: '_avatar3',
  },
  {
    id: 3,
    src: require('~/assets/images/avatars/avatar4.png'),
    srcset: require('~/assets/images/avatars/avatar4.webp'),
    taken: false,
    class: '_avatar4',
  },
  {
    id: 4,
    src: require('~/assets/images/avatars/avatar5.png'),
    srcset: require('~/assets/images/avatars/avatar5.webp'),
    taken: false,
    class: '_avatar5',
  },
  {
    id: 5,
    src: require('~/assets/images/avatars/avatar6.png'),
    srcset: require('~/assets/images/avatars/avatar6.webp'),
    taken: false,
    class: '_avatar6',
  },
]

export const OPPONENT_FOUND_TIME = 15000
export const GAME_OVER_TIME = 2000
