import Vue from 'vue'
import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'
import game from '~/store/game'
import index from '~/pages/index'
import lobby from '~/store/lobby'
import { mount } from '@vue/test-utils'
import user from '~/store/user'

const localVue = createLocalVue()
localVue.use(Vuex)


describe('index page', () => {
  let store
  let wrapper
  let matchmakingLobby
  let personalLobby

  function addClass(item, cl) {
    if (item) {
      setTimeout(() => item.classList.add(cl), 15000)
    }
  }
  beforeEach(() => {
    store = new Vuex.Store({ modules: { lobby, user, game } })
    wrapper = mount(index, { localVue, store })
    matchmakingLobby = wrapper.find('.main__matchmaking')
    personalLobby = wrapper.find('.main__personal')
  })

  afterEach(() => {
    jest.useRealTimers()
  });

  test('is matchmaking lobby hidden initially', () => {
    expect(matchmakingLobby.exists()).toBe(false)
  })
  test('are lobbies changed on click', async () => {
    expect(matchmakingLobby.exists()).toBe(false)
    const button = wrapper.find('.matchmaking-lobby-btn')
    await button.trigger('click')
    expect(personalLobby.classes()).toContain('_hide')
    expect(wrapper.find('.main__matchmaking').classes()).toContain('_show')
  })
  test('is create user modal shown on invite player click', async () => {
    expect(wrapper.find('.user-modal').exists()).toBe(false)
    const button = wrapper.find('.profile__name._invite')
    await button.trigger('click')
    expect(wrapper.find('.user-modal').exists()).toBe(true)
  })
  test('is photo changed on user click', async () => {
    const openProfileButton = wrapper.find('.profile__name._invite')
    await openProfileButton.trigger('click')
    const wr = wrapper.find('.user-modal')
    const arrowIconButton = wr.find('.arrow-icon')
    const image = wr.find('img')
    await arrowIconButton.trigger('click')
    expect(image.classes()).not.toContain('_avatar2')
    expect(image.classes()).toContain('_avatar6')
  })
  test('is opponent found around 15 seconds later', async () => {
    const button = wrapper.find('.matchmaking-lobby-btn')
    await button.trigger('click')
    jest.useFakeTimers()
    addClass(wrapper.find('.main__matchmaking').element, '_found')
    jest.runAllTimers()
    expect(wrapper.find('.main__matchmaking').classes()).toContain('_found')
  })
})


